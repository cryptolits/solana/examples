# Explorer

### [template-sc](../../../-/tree/template-sc)

---
Simple smart contract that can be used as a template to begin development.

### [pda-example](../../../-/tree/pda-example)

---
Smart contract with stake/unstake instructions as the example of how Solana PDA works.

### [reward-pool](../../../-/tree/reward-pool)

---
Staking smart contract with reward pool logic and token smart contract that creates platform's token. 

User can stake any tokens and get unique platforms' nfts as the reward. 

User can stake platforms' nfts and get platform's token as the reward.

### [vrf-test](../../../-/tree/vrf-test)

---
Smart contract as the example of how we can get random number by switchboard-v2 via VRF.

### [vrf-contract](../../../-/tree/vrf-contract)

---
Anchor smart contract to parse VRF.

### [Lottery-machine](../../../-/tree/lottery_machine)

---
Lottery smart contract, that represents logic:
Creating NFTs campaigns and buying tickets for participants of the campaigns.
The draw of results with the random number obtained through Chainlink.

### [Stake-from-anchor](../../../-/tree/stake_from_anchor)

---
Smart contract with stake/unstake logic. 
Rewritten from Anchor 

### [Dynamic_storage](../../../-/tree/dynamic_storage)

---
Smart contract that shows examples of data storing in Solana.
